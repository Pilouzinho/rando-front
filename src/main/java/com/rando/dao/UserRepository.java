package com.rando.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rando.modele.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer>{

}
