package com.rando.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rando.modele.Etape;

@Repository
public interface EtapeRepository extends JpaRepository<Etape, Integer>{

}
