package com.rando.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.rando.modele.Commentaire;

@Repository
public interface CommentaireRepository extends JpaRepository<Commentaire, Integer>{
	
	@Query(value="select * from commentaire c where c.fk_id_etape = ?1", nativeQuery = true)
	List<Commentaire> findByEtapeId(int idEtape);

}
