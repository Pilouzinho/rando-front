package com.rando.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rando.modele.Itineraire;

@Repository
public interface ItineraireRepository extends JpaRepository<Itineraire, Integer>{

}
