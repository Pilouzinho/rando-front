package com.rando.modele;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "itineraire")
public class Itineraire implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_itineraire")
	private int idItineraire;
	
	@Column(name = "nom_itineraire")
	private String nomItineraire;
		
	//@ManyToOne(cascade={CascadeType.DETACH,CascadeType.REFRESH})
	@ManyToOne
	@JoinColumn(name = "niveau_itineraire")
	private Niveau niveauItineraire;
	
	

	public Itineraire() {
		super();
	}

	public Itineraire(int idItineraire, String nomItineraire, String descriptionItineraire, Niveau niveauItineraire) {
		super();
		this.idItineraire = idItineraire;
		this.nomItineraire = nomItineraire;
		this.niveauItineraire = niveauItineraire;
	}

	public Itineraire(String nomItineraire, String descriptionItineraire, Niveau niveauItineraire) {
		super();
		this.nomItineraire = nomItineraire;
		this.niveauItineraire = niveauItineraire;
	}

	public int getIdItineraire() {
		return idItineraire;
	}

	public void setIdItineraire(int idItineraire) {
		this.idItineraire = idItineraire;
	}

	public String getNomItineraire() {
		return nomItineraire;
	}

	public void setNomItineraire(String nomItineraire) {
		this.nomItineraire = nomItineraire;
	}

	

	public Niveau getNiveauItineraire() {
		return niveauItineraire;
	}

	public void setNiveauItineraire(Niveau niveauItineraire) {
		this.niveauItineraire = niveauItineraire;
	}

	@Override
	public String toString() {
		return "Itineraire [idItineraire=" + idItineraire + ", nomItineraire=" + nomItineraire
				+ ", niveauItineraire=" + niveauItineraire + "]";
	}
	
	
}
